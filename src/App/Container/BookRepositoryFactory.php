<?php
/**
 * Created by PhpStorm.
 * User: mateuszbielinski
 * Date: 15.09.2016
 * Time: 09:04
 * Contact: kontakt@mateuszbielinski.pl
 */

namespace App\Container;


use App\Entity\Book;
use Interop\Container\ContainerInterface;

class BookRepositoryFactory
{
    public function __invoke(ContainerInterface $container)
    {
        return $container->get('doctrine.entity_manager.orm_default')
            ->getRepository(Book::class);
    }
}