<?php
/**
 * Created by PhpStorm.
 * User: mateuszbielinski
 * Date: 15.09.2016
 * Time: 10:16
 * Contact: kontakt@mateuszbielinski.pl
 */

namespace App;


use Doctrine\Common\Persistence\Mapping\Driver\MappingDriverChain;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;

class ConfigProvider
{
    public function __invoke()
    {
        return[
            'doctrine' => $this->getDoctrineConfig(),
        ];
    }

    private function getDoctrineConfig()
    {
        return [
            'driver' => [
                'orm_default' => [
                    'class' => MappingDriverChain::class,
                    'drivers' => [
                        'App\Entity\Book'=>'book_entity'
                    ],
                ],
                'book_entity' => [
                    'class' => AnnotationDriver::class,
                    'cache' => 'array',
                    'paths' => __DIR__.'/Entity'
                ],
            ],
        ];
    }
}