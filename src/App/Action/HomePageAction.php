<?php

namespace App\Action;

use App\Entity\Repository\BookRepository;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\JsonResponse;
use Zend\Expressive\Router;
use Zend\Expressive\Template;
use Zend\Expressive\Plates\PlatesRenderer;
use Zend\Expressive\ZendView\ZendViewRenderer;

class HomePageAction
{
    private $router;

    private $template;

    private $bookRepository;

    public function __construct(Router\RouterInterface $router, Template\TemplateRendererInterface $template = null, BookRepository $bookRepository)
    {
        $this->router   = $router;
        $this->template = $template;
        $this->bookRepository = $bookRepository;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        return new JsonResponse($this->bookRepository->findAll());
    }
}
