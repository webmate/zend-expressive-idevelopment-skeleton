<?php

use Zend\Expressive\ConfigManager\ConfigManager;
use Zend\Expressive\ConfigManager\PhpFileProvider;
use App\ConfigProvider;

$configManager = new ConfigManager(
    [
        ConfigProvider::class,
        new PhpFileProvider('config/autoload/{{,*.}global,{,*.}local}.php'),
    ]
);

return $configManager->getMergedConfig();