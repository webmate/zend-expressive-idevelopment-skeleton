<?php
/**
 * Created by PhpStorm.
 * User: mateuszbielinski
 * Date: 14.09.2016
 * Time: 14:57
 * Contact: kontakt@mateuszbielinski.pl
 */
$container = require 'container.php';

return new \Symfony\Component\Console\Helper\HelperSet([
    'em' => new \Doctrine\ORM\Tools\Console\Helper\EntityManagerHelper(
        $container->get('doctrine.entity_manager.orm_default')
    ),
]);