<?php
/**
 * Created by PhpStorm.
 * User: mateuszbielinski
 * Date: 15.09.2016
 * Time: 09:22
 * Contact: kontakt@mateuszbielinski.pl
 */

return [
    'dependencies' => [
        'factories' => [
            'doctrine.entity_manager.orm_default' => \ContainerInteropDoctrine\EntityManagerFactory::class,
        ],
    ],
    'doctrine' => [
        'driver' => [
            'orm_default' => [
                'class' => \Doctrine\Common\Persistence\Mapping\Driver\MappingDriverChain::class,
                'drivers' => [
          //          'App\Entity\Book' => 'book_entity',
                ],
            ],
        ],
    ],

];